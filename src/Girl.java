class Girl {
    private Flower flower;
    private short age;

    public Flower getFlower() {
        return flower;
    }

    public void recieveFlower(Flower flower) {
        this.flower = flower;
    }

    public short age() {
        return age;
    }

    public void setAge(short age) {
        this.age = age;
    }
}
