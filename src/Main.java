public class Main {

    public static void printFigure(Figure figure) {
        figure.print();
    }

    public static void main(String[] args) {

        Square square = new Square();
        Triangle triangle = new Triangle();

        printFigure(square);

        System.out.print("\n\n\n");

        printFigure(triangle);
    }
}
