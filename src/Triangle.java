public class Triangle extends Figure {

    public void print() {
        for (int height = 0; height < 5; height++) {
            for (int width = 0; width < 5; width++) {
                if (height >= width) {
                    System.out.print(" 0 ");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.print('\n');
        }
    }
}

